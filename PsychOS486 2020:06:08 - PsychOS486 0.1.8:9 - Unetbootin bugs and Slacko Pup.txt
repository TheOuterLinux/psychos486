####################################################################
# 2020/06/08 - PsychOS486 0.1.8:9 - Unetbootin bugs and Slacko Pup #
####################################################################

I used 'Unetbootin' to place 'slacko-5.3.1-SCSI-MAIN.iso' onto a USB
and then edit the file so it looks like a distro called "PsychOS486"
when booting. I also made sure to run:

    sudo parted /dev/sdc set 1 boot on
    
...so it would actually be bootable because of a 'Unetbootin' bug. The
plan from there is to boot the ISO, make changes, and then use
"/usr/sbin/remasterpup2" to hopefully create custom ISO's with permanent
changes instead of asking people to run a save file. I would, however,
like to note that the boot menu at the beginning looks different from a
USB than it does on a CD and the 'logo.16' image was created using the 
original and then using 'lss16toppm' and 'ppmtolss16' commands. You must
use the classic CGA 16 colors or it will look funny or not at all.
