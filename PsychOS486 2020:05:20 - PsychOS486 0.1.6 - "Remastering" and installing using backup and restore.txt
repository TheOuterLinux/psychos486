#########################################################################################
# 2020/05/20 - PsychOS486 0.1.6 - "Remastering" and installing using backup and restore #
#########################################################################################

I installed 'Slackware 14.1', added 'GrafX2', and then changed a few 
things. The next step now is to 'dd + gzip + split' the entire "/dev/sda"
and see the size and file number results.

Command used:

    dd if=/dev/sda conv=sync,noerror bs=128k | gzip -c | split - "/path/to/USBStorage/PsychOS486.img.gz"

This is what should be used to restore:

    cat "/path/to/USBStorage/PsychOS486.img.gz*" | gzip -dc | dd of=/dev/sda bs=128k

The MBR (Master Boot Record) should get backed-up along with all of this,
but just in case:

    dd if=/dev/sda of /tmp/sdambr.img bs=512 count=1

This basically just copies the first 512 bytes of the HD, which is where
the MBR information is stored.

[A little while later...]

The 'dd + gzip + split' idea created 14 files for a total of 8.75GB.
What I don't understand is why 'df -h' shows only 4.8GB used. No wait,
just double-checked the output and it actually didn't finish and ran out
of room on the USB storage device I was using. I'm going to try to have
the gzipped image on the system itself this time instead of putting them
on an external hard drive and see what happens. If I try to backup the
whole hard drive, does it also backup mounted partitions from external
hard drives? That might be the problem.

I'm also going to switch the bs=128k to bs=4k (block size) based on
https://www.mail-archive.com/eug-lug@efn.org/msg12073.html as it is
from December 2002 and should be me "accurate." Apparently, 'cfdisk' on
Debian 3.0 uses a default block size of 4k. I'll also be running the 
'time' command along with it to see how long this takes.

I just realized why the file size of the backup using 'dd' was so large
and that was because it needs the count option or it will copy the entire
hard drive and not just the used space. On a 160GB hard drive, I ran 
'fdisk -l' and /dev/sda listed End as being 312581807. 'fdisk -l' also
uses 512 for the blocksize. So, I'm going to try bs=512 count=31258108;
you add one more to the count to capture the last block. If this does 
work, people may have to use GParted to expand the partition because 
there may be unallocated free space.

[A little while later...]

That also did not go as expected, so I'm going to try using 'tar' and 
see what size I get...

    tar -cvpzf - --exclude=- --one-file-system / | split -b 600MiB - "$HOME/PsychOS486.tar.gz"

...That produced 6 files with a total of 3.4GB with each file being
around 629.1MB in size, except for the last one at 289.1MB in size.

To restore:

    tar -xvpzf "/path/to/backup.tar.gz" -C "/restore/location" --numeric-owner

As far as recovery goes, BG-Rescue (http://www.giannone.ch/rescue) might
do the trick. I can use it to restore and then run 'sudo lilo -M /dev/sda mbr'
to fix the MBR.
