############################################
# 2020/04/11 - PsychOS486 - Slackware 14.1 #
############################################

Installed Slackware 14.1 and though it was all done from the command-line,
it was fairly easy. However, it only asks about root, so you have to 
create your own user. I did this via:

    useradd -m -g users -G wheel,floppy,audio,video,cdrom,plugdev,power,netdev,lp,scanner -s /bin/bash psychos
    
Next, you create a password using "passwd psychos"; "linux" was used.
You can then log out of root and then log into "psychos" and run 'startx'
to launch XFCE.

I used one of the auto-mirror options in "/etc/slackpkg/mirrors" to update
and it took a while and apparently, you are only supposed to enable one
repository at a time.

I had issues mounting USB storage devices but adding the user "psychos"
to the "disks" group and adding "psychos ALL(ALL) ALL" to "/etc/sudoers"
and a reboot solved the issue.

Also, if you plan on having Internet, I highly recommend setting it up 
when asked during the install.

I added 'sbopkg' for adding more software. It's pretty cool, but you have
to remember to switch it to use the 14.1 repository. There are some 14.2
source packages that will compile for i486 if you edit the SlackBuild
file.
