##########################################################################
# 2020/06/13 - PsychOS486 1.2.0 - Created ISO booting on QEMU only issue #
##########################################################################

So... I customized 'slacko-5.3.1-SCSI-MAIN.iso' while booted and then 
used the 'remasterpup2'command to create an ISO. However, that ISO it
generated either only boots on QEMU or there is something wrong with the
disc burns. The original slacko iso boots just fine on QEMU from the ISO
(-cpu 486) and from a DVD on an i686 laptop. The 'remasterpup2' has
options for both burning to a disc and creating an ISO and I might try
the burn to a dsic option and see if it will still give me issues.

Also, I am going to start these PsychOS486 version numbers with "1.x"
now instead of zero.
