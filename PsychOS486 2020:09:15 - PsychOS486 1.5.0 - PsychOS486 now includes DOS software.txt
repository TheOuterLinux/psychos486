########################################################################
# 2020/09/15 - PsychOS486 1.5.0 - PsychOS486 now includes DOS software #
########################################################################

I've been making a bunch of little tweaks and installs here and there,
especially after discovering an i486 version of DOSEMU that works. This
emulator will even run from the console as long as 'xorgwizard' is ran
first. This could potentially mean having some sort of weird Linux/DOS
hybrid-like system that would help extend PsychOS486's capabilities much
further than 'WINE' already has, which oddly enough does not run DOS
software. The only downside I have noticed with DOSEMU so far is some
GUI programs like 'AnimatorAKA' do not behave quite right in regards to
the mouse.

I've also managed to find some video game console emulators that will
end-up being a unique mixture of GNU/Linux, DOS, and WINE software.

As far as scripts that come with PsychOS 3.x series like 'CLIMax' and
'QuickEdit', I'll be focusing on those last since I need to see how much
software I can get a hold of to know what to add, remove, or replace
feature-wise.
