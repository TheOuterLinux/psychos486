####################################################
# 2020/04/23 - PsychOS486 0.0.2:4 - linux-live 1.8 #
####################################################

[0.0.2]

I tried reinstalling llvm and change the config file in 'linux-live-2.3'
from "VMLINUZ=/vmlinuz" to "VMLINUZ=/boot/vmlinuz-huge-3.10.107", 
thinking that since technically the "/boot/vmlinuz" is a symbolic link,
that may be the issue; however, it didn't work but at least the custom
bootlogo.png (./PsychOS486%202020:04:23%20-%20PsychOS486%200.0.2%20-%20BootImg.png) 
I made showed.

I did find out from a lucky/randomly found PDF during an Internet search
that Slackware 14.2 comes with script to make custom ISO's and such but 
seems to require kernel version 4.x but I'm not 100% sure yet and I'm 
thinking about borrowing these scripts from 14.2 to try to run on 14.1 
and see what happens.

[0.0.3]

I partially figured out the issue with the boot menu and that is it is
looking for a file called "/linux/boot/vmlinuz", so I used ISO Master to
rename "vmlinux-huge-3.10.107" to simply "vmlinux" and it sort of worked
but now there is some sort of 'aufs' issue, I think.

According to https://linux-live.org/#changes, 'aufs' is used instead of
'unionsfs' and that might be why. So, I'm going to try using 'linux-live-1.8'
(Jan 4th, 2013) version instead and see if it behaves differently. I also
noticed binaries within its "./intramfs/static" folder with "i486" in 
their names so I'm taking that as a good sign, but we'll see.

[0.0.4]

Currently waiting on 'linux-live-1.8' to build an ISO. It didn't come
with a config file even though its README mentions editing one so I'm
using the one from 'linux-live-2.3. Also, I'm using 'vmlinux-huge-3.10.107-smp' 
this time if the defaults don't work for 1.8 since apparently, that's 
what the 'vmlinuz' symbolic link on the system was actaully using. However, 
'*-smp' is for motherboards with multiple processors on them and using 
'smp' may slow things down on older computers.

When the build script for 'linux-live-1.8 was finished, it created both
an ISO and a ZIP. Booting from a DVD lead to the custom boot menu which
then lead to "/" (root) with nothing inside of "/usr" and no 'startx'.
Also, I just found out that the config file was there for 1.8 but it was
hidden.
