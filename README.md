#PsychOS486
[https://psychoslinux.gitlab.io/486/index.html](https://psychoslinux.gitlab.io/486/index.html)

##Code named "Wilfred"


- **Default username:** root
- **Default password:** woofwoof
- **Initial release date:** 2020/11/28
- **Version:** 1.6.5
- **Target:** Older i486DX/i586/i686 computers with CD-ROM drives
- **Recommended RAM minimum for console-only use:** 32MB (will not boot below 23MB)
- **Recommended RAM minimum for GUI desktop, but you'll hate it:** 64MB
- **RAM minimum for over-all mental well-being:** 128MB
    Download Size: 687.6MB
- **Installation Size:** 4.71GB on EXT2 file-system; GUI method only
- **Supports persistence:** Yes
- **MD5:** dc6b6841766f83621af254ac88ab98bd
- **License:** It might be [LGPL](http://wikka.puppylinux.com/License)?
- **E-Mail:** [psychosgnulinux@protonmail.com](mailto:psychosgnulinux@protonmail.com)


[CLICK TO VOTE FOR PSYCHOS ON DISTROWATCH](https://distrowatch.com/dwres.php?waitingdistro=551&resource=links#new)

####Why?!

Why not? SSL is everywhere and if these repositories one day decided to switch to https completely, the older GNU/Linux distributions are screwed, even more so than usual. On top of that, adding or compiling newer versions of software is almost a joke due to GLIBC errors, libc, and other dependencies, even if you try to use something like [sbopkg](https://sbopkg.org/). My point is, I had to do something like this now if I ever wanted PsychOS to have an i486DX[#] compatible version, if nothing more than just for bragging-rights on a technicality. In other words, it may not run the way you are hoping for, but it will do something. What is left of the i586 users should not have anything to worry about.

###Future Updates

I'm not going to "sugar coat it." If there are going to be updates to PsychOS486, it will probably be mostly in regards to the [PsychOS486Tools](https://psychoslinux.gitlab.io/486/psychos486tools.tar.gz) (/opt/psychos486tools) and not much else. Also, for now, if there are any full-version updates, they would be focused on the listed issues at the end of the [release notes](https://psychoslinux.gitlab.io/486/DOCS/LookHere/PsychOS486_ReleaseNotes.txt). Updating the included software for this distro is also nearly impossible, but you are more than welcome to try and let me know how either by [E-Mail](mailto:psychosgnulinux@protonmail.com) or reporting an [issue](https://gitlab.com/PsychOSLinux/psychoslinux.gitlab.io/issues).

###Why Slacko Puppy 5.3.1?

I originally wanted to be able to use [Slackware 14.1](https://slackware.uk/slackware/slackware-iso/slackware-14.1-iso/) because of it mostly using i486 software and worked well with sbopkg for adding more. However, the ability to respin Slackware without fail seems to be limited to 14.2 (i586) and newer due to the Linux kernel 4.x series as a requirement and probably because Slackware 14.1 isn't live. So, after trying [SEVERAL GNU/Linux distributions](https://theouterlinux.gitlab.io/Public/Articles/i486.txt), I settled on Slacko Puppy 5.3.1, which is a Puppy version of Slackware 13.37. Having a menu option to remaster a CD or ISO is nice. For more details on my blood, sweat, and tears, you can check out the [development notes](https://psychoslinux.gitlab.io/486/DOCS/PsychOS486DevLogs/DescendingIndex.html).

###A message for the "Security Sticklers"...

The PsychOS486 distro currently uses an older kernel. This is the default kernel that came with Slacko Puppy 5.3.1 and I did try to compile a pre-Spectre/Meltdown (because of speed), 4.x kernel ([see dev logs 2020/06/28 thru 2020/07/05](https://psychoslinux.gitlab.io/486/DOCS/PsychOS486DevLogs/DescendingIndex.html)) for potential remastering capability reasons, but it caused too many issues, so I gave up on the idea. So, in the interest of "no one cares about your i486DX/i586 computer enough to repel from the ceiling to steal your I'm lonely collection," I am just going to keep it as it is, for now. I also have a hard time imagining anyone using this distro to run a public web server, but who am I to judge? And if you must, you can find details on potential security issues regarding this kernel version [here](https://www.cvedetails.com/vulnerability-list.php?vendor_id=33&product_id=47&version_id=123877&page=1&hasexp=0&opdos=0&opec=0&opov=0&opcsrf=0&opgpriv=0&opsqli=0&opxss=0&opdirt=0&opmemc=0&ophttprs=0&opbyp=0&opfileinc=0&opginf=0&cvssscoremin=0&cvssscoremax=0&year=0&cweid=0&order=1&trc=233&sha=5674e3a2cdf7026e19371169e4d4c5e3f2dce6b5).
